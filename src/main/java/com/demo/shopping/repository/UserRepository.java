package com.demo.shopping.repository;

import java.util.List;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.shopping.entity.Cart;
import com.demo.shopping.entity.Product;
import com.demo.shopping.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	
	
}
