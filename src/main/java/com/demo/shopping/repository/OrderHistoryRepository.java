package com.demo.shopping.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.shopping.entity.Orders;
import com.demo.shopping.entity.User;

@Repository
public interface OrderHistoryRepository extends CrudRepository<Orders, Long> {

	List<Orders> findByUser(User user);

}
