package com.demo.shopping.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.shopping.entity.Cart;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {
	
	
}
