package com.demo.shopping.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.shopping.entity.User;

@Repository
public interface LoginRepository extends CrudRepository<User, Long> {
	
	
	@Query(value = "select * FROM User u WHERE u.email = :email AND u.password = :password", nativeQuery = true)
	public Optional<User> findStudentByEmailAndPasswordParams(@Param("email") String email, @Param("password") String password);

}
