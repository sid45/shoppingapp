package com.demo.shopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.shopping.entity.Product;

@Repository
public interface ProductsRepository extends CrudRepository<Product, Long> {
	
	/*
	 * @Query(value = "SELECT * FROM Product u WHERE u.quantity < 1", nativeQuery =
	 * true) List<Product> findAllProductsAvailabilityFalse();
	 */

	List<Product> findByProductNameIgnoreCaseContaining(String productName);

}
