package com.demo.shopping.exception;

public class ProductNotFoundException extends Exception {

	public ProductNotFoundException(String message) {
		super(message);
	}
	
	

}
