package com.demo.shopping.constants;

public class AppConstants {
	
	
	public static final String 	PRODUCT_NOT_FOUND = "product not found";
	
	public static final String LOGIN_SUCCESS = "LOGIN SUCCESS";
	
	public static final String LOGIN_FAILURE = "LOGIN FAILED";

	public static final String USER_NOT_FOUND = "user not found";

}
