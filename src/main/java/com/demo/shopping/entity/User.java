package com.demo.shopping.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="User")
public class User implements Serializable {
	
	@Id
	private long userId;
	
	private String userName;
	
	private String email;
	
	private String password;

}
