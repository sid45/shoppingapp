package com.demo.shopping.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cartProductDetail")
public class CartProductDetail {
	@Id
	private long cartProductDetailId;
	

	@ManyToOne
	@JoinColumn(name="cart_id")
	private Cart cartId;
    
	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;
	
	private int productQuantity;
}
