package com.demo.shopping.entity;


import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cart")
public class Cart implements Serializable {
	
	@Id
	private long cartId;
	private String cartStatus;
	
	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	}
