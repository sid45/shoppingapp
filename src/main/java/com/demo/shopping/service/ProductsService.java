package com.demo.shopping.service;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.shopping.dto.ProductResponseDto;
import com.demo.shopping.dto.PurchaseRequestDto;
import com.demo.shopping.entity.Cart;
import com.demo.shopping.entity.CartProductDetail;
import com.demo.shopping.entity.Orders;
import com.demo.shopping.entity.Product;
import com.demo.shopping.entity.User;
import com.demo.shopping.exception.ProductNotFoundException;
import com.demo.shopping.repository.CartProductDetailRepository;
import com.demo.shopping.repository.CartRepository;
import com.demo.shopping.repository.OrderRepository;
import com.demo.shopping.repository.ProductsRepository;
import com.demo.shopping.repository.UserRepository;

@Service
public class ProductsService {
	@Autowired
	ProductsRepository productsRepository;
	
	@Autowired
	CartProductDetailRepository cartProductDetailRepository;
	
	@Autowired
	CartRepository cartRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	Product product = new Product();

	public List<ProductResponseDto> productList(String productName, Boolean avaiability) throws ProductNotFoundException {
		// TODO Auto-generated method stub
		List<ProductResponseDto> productResponseDtoList = new ArrayList<>();

		List<Product> products = productsRepository.findByProductNameIgnoreCaseContaining(productName);

		if (products.isEmpty()) 
			throw new ProductNotFoundException("not found any products with this name");
		

		for (Product product : products) {
			if (avaiability == true) {
				if (product.getQuantity() >= 1) {
					ProductResponseDto productResponseDto = new ProductResponseDto();
					BeanUtils.copyProperties(product, productResponseDto);
					productResponseDtoList.add(productResponseDto);
				}
			}
			if (avaiability == false) {
				if (product.getQuantity() < 1) {
					ProductResponseDto productResponseDto = new ProductResponseDto();
					BeanUtils.copyProperties(product, productResponseDto);
					productResponseDtoList.add(productResponseDto);
				}
			}
																																																																																																																																																																																																																																													
		}
		return productResponseDtoList;

	}

	public String purchaseProduct(PurchaseRequestDto purchaseDto) {
		// TODO Auto-generated method stub
		
 Orders orders=new Orders();
    Optional<Cart> cart = cartRepository.findById(purchaseDto.getCartId());
    
    if(cart.isPresent())
      orders.setCart(cart.get());
    
     Optional<User> user = userRepository.findById(purchaseDto.getUserId());
 
     if(user.isPresent())
    	 orders.setUser(user.get());  
     
     orders.setCartTotal(purchaseDto.getCartTotal());
     orders.setOrderStatus(purchaseDto.getOrderStatus());
     
		
		/*
		 * List<CartProductDetail> cartProductDetail =
		 * cartProductDetailRepository.findAllByCartId(cart.get());
		 * 
		 * if(!cartProductDetail.isEmpty()) {
		 * 
		 * for (CartProductDetail cartProductDetail2 : cartProductDetail) {
		 * 
		 * product=
		 * productsRepository.findById(cartProductDetail2.getProduct().getProductId());
		 * cartProductDetail2.getProductQuantity() }
		 * 
		 * }
		 */
		 
     
       orderRepository.save(orders);
     
		return "ordered successfully";
	}

}
