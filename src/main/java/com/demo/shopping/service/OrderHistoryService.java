package com.demo.shopping.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.shopping.dto.OrderHistoryResponseDto;
import com.demo.shopping.dto.OrdersResponse;
import com.demo.shopping.entity.Orders;
import com.demo.shopping.entity.User;
import com.demo.shopping.exception.UserNotFoundException;
import com.demo.shopping.repository.LoginRepository;
import com.demo.shopping.repository.OrderHistoryRepository;

/**
 * 
 * @author siddu
 *
 */
@Service
public class OrderHistoryService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderHistoryService.class);

	
	@Autowired
	private OrderHistoryRepository orderHistoryRepository;
	
	@Autowired
	private LoginRepository loginRepository;
	
	/**
	 * 
	 * @param userId
	 * @return OrderHistoryResponseDto.
	 * @throws UserNotFoundException
	 */
	public OrderHistoryResponseDto userOrderHistory(Long userId) throws UserNotFoundException {
		LOGGER.info("inside userOrderHistory method");
		OrderHistoryResponseDto responseDto = new OrderHistoryResponseDto();
		Optional<User> optionalUser = loginRepository.findById(userId);
		List <Orders> ordersList = null;
		if (optionalUser.isPresent()) {
			ordersList = orderHistoryRepository.findByUser(optionalUser.get());
			responseDto.setUserId(optionalUser.get().getUserId());
			
			/*
			 * Function mapper=new Function<Orders, List<OrdersResponse>>() { List
			 * <OrdersResponse> orderResponseList = new ArrayList<OrdersResponse>();
			 * 
			 * @Override public List<OrdersResponse> apply(Orders t) { OrdersResponse
			 * ordersResponse = new OrdersResponse();
			 * ordersResponse.setOrderId(t.getOrderId());
			 * ordersResponse.setCartTotal(t.getCartTotal());
			 * ordersResponse.setOrderStatus(t.getOrderStatus());
			 * orderResponseList.add(ordersResponse); return orderResponseList; }
			 * 
			 * };
			 */
			//List<OrdersResponse>orderResponseList1 = (List<OrdersResponse>) ordersList.stream().map(mapper).collect(Collectors.toList());

			List<OrdersResponse> orderResponseList1 = new ArrayList<OrdersResponse>();
			for (Orders orders1 : ordersList) {
				OrdersResponse orderResponse = new OrdersResponse();
				orderResponse.setOrderId(orders1.getOrderId());
				orderResponse.setCartTotal(orders1.getCartTotal());
				orderResponse.setOrderStatus(orders1.getOrderStatus());
				orderResponseList1.add(orderResponse);
			}
			responseDto.setOrderList(orderResponseList1);
		} else {
			throw new UserNotFoundException("user not found");
		}
		LOGGER.info("exiting userOrderHistory method");
		return responseDto;
		
		
	}
	
	

}
