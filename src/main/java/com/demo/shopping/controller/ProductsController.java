package com.demo.shopping.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.shopping.dto.ProductResponseDto;
import com.demo.shopping.dto.PurchaseRequestDto;
import com.demo.shopping.exception.ProductNotFoundException;
import com.demo.shopping.service.ProductsService;

@RestController
@RequestMapping("/shopping")
public class ProductsController {
	@Autowired
	ProductsService productsService;

	@RequestMapping("/hello")
	public String hello() {
		return "hello";
	}

	@GetMapping(value = "/searchProducts/productName/avaiability")
	public ResponseEntity<List<ProductResponseDto>> productList(@RequestParam String productName,
			@RequestParam Boolean avaiability) throws ProductNotFoundException {

		// logger.info("Inside ProductsController of productsList method ");
		return new ResponseEntity<>(productsService.productList(productName, avaiability), HttpStatus.ACCEPTED);
	}

	@PostMapping(value = "/purchaseProduct")
	public ResponseEntity<String> purchaseProduct(@RequestBody PurchaseRequestDto purchaseDto) {

		return new ResponseEntity<>(productsService.purchaseProduct(purchaseDto), HttpStatus.OK);
	}

}
