package com.demo.shopping.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.shopping.dto.OrderHistoryResponseDto;
import com.demo.shopping.exception.UserNotFoundException;
import com.demo.shopping.service.OrderHistoryService;


/**
 * 
 * @author sidramesh mudhol
 *
 */
@RestController
@RequestMapping("/api")
public class OrderhistoryController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderHistoryService.class);

	@Autowired
	private OrderHistoryService orderHistoryService;
	
	/**
	 * 
	 * @param userid
	 * @return ResponseEntity<OrderHistoryResponseDto>
	 * @throws UserNotFoundException
	 */
	@RequestMapping("/orderhistory")
	public ResponseEntity<OrderHistoryResponseDto> userOrderHistory(@RequestParam  Long userid) throws UserNotFoundException {
		LOGGER.info("inside userOrderHistory method");
		OrderHistoryResponseDto responseDto = orderHistoryService.userOrderHistory(userid);
		LOGGER.info("exiting userOrderHistory method");
		return new ResponseEntity<OrderHistoryResponseDto>(responseDto, HttpStatus.OK);
		
		
	}

}
