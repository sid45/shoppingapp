package com.demo.shopping.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.shopping.constants.AppConstants;
import com.demo.shopping.dto.LoginRequestDto;
import com.demo.shopping.dto.LoginResponseDto;
import com.demo.shopping.entity.User;
import com.demo.shopping.service.LoginService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class LoginController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private LoginService loginService;

	
	
	@PostMapping("/login")
	@ApiOperation("Student login")
	public LoginResponseDto studentLogInValidation(@RequestBody LoginRequestDto request) {
		LOGGER.info("student login cred===" + request.getEmail()+","+request.getPassword());
		LoginResponseDto response = new LoginResponseDto();
		LOGGER.info("!!!!!!!!!!Before calling service classS!!!!!!!!!");
		Optional<User> student = loginService.loginValidation(request.getEmail(), request.getPassword());
		if(student.isPresent()) {
			LOGGER.debug("student.get().== " + student.get());
			response.setResponseCode("1");
			response.setStatus(AppConstants.LOGIN_SUCCESS);
			response.setErrorMessage("");
		}else {
			LOGGER.warn("please enter the correct username and password");
			response.setResponseCode("0");
			response.setStatus(AppConstants.LOGIN_FAILURE);
			response.setErrorMessage("please enter correct username and password");
		}
		LOGGER.info("************************After service call**********************");
		return response;
	}
	

}
