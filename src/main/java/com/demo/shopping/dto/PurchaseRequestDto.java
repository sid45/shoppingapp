package com.demo.shopping.dto;

import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class PurchaseRequestDto {
	
	private String orderStatus;
	private long cartId;
	private long userId;
	private float cartTotal;

}
