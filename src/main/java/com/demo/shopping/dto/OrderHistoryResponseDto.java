package com.demo.shopping.dto;

import java.util.List;

import com.demo.shopping.entity.Orders;


/**
 * 
 * @author sidramesh mudhol
 *
 */
public class OrderHistoryResponseDto {
	
	private long userId;
	
	private List<OrdersResponse> orderList;

	
	
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public List<OrdersResponse> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrdersResponse> orderList) {
		this.orderList = orderList;
	}

	
	
	
	

}
