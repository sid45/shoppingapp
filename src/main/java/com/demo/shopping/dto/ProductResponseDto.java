package com.demo.shopping.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductResponseDto {

private String productName;
private int quantity;

}
