package com.demo.shopping.controllerTest;

import java.util.ArrayList;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.demo.shopping.controller.ProductsController;
import com.demo.shopping.dto.ProductResponseDto;
import com.demo.shopping.dto.PurchaseRequestDto;
import com.demo.shopping.exception.ProductNotFoundException;
import com.demo.shopping.service.ProductsService;

@RunWith(MockitoJUnitRunner.class)
public class ProductControllerTest {

	@Mock
	ProductsService productsService;

	@InjectMocks
	ProductsController productsController;

	List<ProductResponseDto> productListResponseDto;
	ProductResponseDto productResponseDto;
	PurchaseRequestDto purchaseRequestDto;

	@Before
	public void setup() {
		productResponseDto = new ProductResponseDto();

		productListResponseDto = new ArrayList<ProductResponseDto>();

		productResponseDto.setProductName("pen");
		productResponseDto.setQuantity(34);

		productListResponseDto.add(productResponseDto);
		
		purchaseRequestDto= new PurchaseRequestDto();
		
		purchaseRequestDto.setCartId(1);
		purchaseRequestDto.setCartTotal(20000);
		purchaseRequestDto.setOrderStatus("true");
		purchaseRequestDto.setUserId(1);
	
	}

	@Test
	public void productListTest() throws ProductNotFoundException {

		Mockito.when(productsService.productList(Mockito.anyString(), Mockito.anyBoolean()))
				.thenReturn(productListResponseDto);
		ResponseEntity<List<ProductResponseDto>> actualValue = productsController.productList(Mockito.anyString(),
				Mockito.anyBoolean());
		Assert.assertEquals(1, actualValue.getBody().size());

	}

	@Test
	public void purchaseProductTest() {

		Mockito.when(productsService.purchaseProduct(Mockito.any(PurchaseRequestDto.class))).thenReturn("ordered successfully");

		ResponseEntity<String> response = productsController.purchaseProduct(purchaseRequestDto);
		Assert.assertEquals(productResponseDto, response.getBody());

	}

}
